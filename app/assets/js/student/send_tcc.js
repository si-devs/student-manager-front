/*!
 * ManagerStudent
 * Application for the course Sistema para Internet
 * https://gitlab.com/si-devs/student-manager-front
 * @author Jaqueline Paschoal and Robson Formigão
 * @version 1.1.1
 * Copyright 2018. MIT licensed.
 */
(function(config){

  'use strict';

  var config = config();  
  var configApi = config.api();

  var $buttonSendTcc = document.querySelector("[data-js='button-send-tcc']");
  var formData = new FormData();

  if($buttonSendTcc) {
    $buttonSendTcc.addEventListener('click', function(e) {
      e.preventDefault();
      
      var $inputFileTcc = document.querySelector("[data-js='input-file-tcc']").files;
      var $inputTitle = document.querySelector('[data-js="input-title"]').value;
      var $inputYear = document.querySelector('[data-js="input-year"]').value;
      var $inputLinkSystem = document.querySelector('[data-js="input-link-system"]').value;
      var id_student = sessionStorage.getItem('id_student');

      function sendFile() {
        var formData = new FormData();
        formData.set("file", $inputFileTcc[0], $inputFileTcc[0].name);

        axios({
          method: 'post',
          url: configApi.urlApi + 'student/send_tcc/' + id_student,
          headers: { 
            'Content-Type': 'multipart/form-data',
            student_token: sessionStorage.getItem("student_token"),
            ra: sessionStorage.getItem('ra') 
          },
          params: {
            title: $inputTitle,         
            year: $inputYear,           
            link_system: $inputLinkSystem  
          },
          data: formData, 
          onUploadProgress: function (progressEvent) {
            config.validate('Verificando...','[data-js="input-file-tcc"]','await');
          }
        })
        .then(function (response) {
          return config.validate('TCC enviado com sucesso para aprovação.','[data-js="input-file-tcc"]','success');
        })
        .catch(function (error) {
          console.log(error.response.data);
          validate(error.response.data);
        });
      }

      function validate(error) {  
        
        if(error.year) {
          return config.validate('Formato de ano inválido, exemplo: 2018', '[data-js="input-file-tcc"]','error');
        }

        if(error.title || error.link_system) {
          return config.validate('Informe todos os campos.', '[data-js="input-file-tcc"]','error');
        }

        if(error.fail) {
          return config.validate('Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :(', '[data-js="input-file-tcc"]','error');
        }

        if(error.invalid) {
          return config.validate('Você já solicitou a aprovação do TCC, caso tenha enviado errado, contate o administrador!', '[data-js="input-file-tcc"]','error');
        }

        if(error.file[0] === 'The file failed to upload.' || error.file[0] === 'The file field is required.' || error.file[0] === 'The file may not be greater than 1000 kilobytes.') {
          return config.validate('Falha ao enviar o arquivo, verifique seu tamanho. (Tamanho max: 5MB)', '[data-js="input-file-tcc"]','error');
        }

        if(error.file[0] === 'The file must be a file of type: pdf.') {
          return config.validate('O arquivo enviado deve ser do tipo PDF.', '[data-js="input-file-tcc"]','error');
        }
      }

      function emptyFile() {
        return config.validate('Você precisa selecionar um arquivo.', '[data-js="input-file-tcc"]','error');
      }

      $inputFileTcc.length ? sendFile() : emptyFile();
      
      
    });
  }

})(window.config)
/*!
 * ManagerStudent
 * Application for the course Sistema para Internet
 * https://gitlab.com/si-devs/student-manager-front
 * @author Jaqueline Paschoal and Robson Formigão
 * @version 1.1.1
 * Copyright 2018. MIT licensed.
 */
(function (config) {

  'use strict';

  var config = config();  
  var configApi = config.api();
    
  var $buttonLoginStudent = document.querySelector('[data-js="btn-login-student"]');
  var $buttonLoginManager = document.querySelector('[data-js="btn-login-manager"]');

  if($buttonLoginStudent) {
    $buttonLoginStudent.addEventListener('click',function(e){
      e.preventDefault();
      e.stopPropagation();
  
      var $ra = document.querySelector("[data-js='ra']").value;
      var $password = document.querySelector("[data-js='password']").value;
      var url = this.href;
  
      axios.post( configApi.urlApi + 'student/', {
        ra: $ra,
        authentication: $password
      })
      .then(function (response) {
        sessionStorage.setItem('student_token', response.data.student_token);
        sessionStorage.setItem('ra', response.data.ra);
        sessionStorage.setItem('id_student',response.data.id_student);
        window.location.href =  url; 
      })
      .catch(function (error) {
        validate(error.response.data);
      });
    }, true);
  }

  if($buttonLoginManager) {
    $buttonLoginManager.addEventListener('click',function(e){
      e.preventDefault();
      e.stopPropagation();
  
      var $login = document.querySelector("[data-js='login']").value;
      var $password = document.querySelector("[data-js='password']").value;
      var url = this.href;
  
      axios.post( configApi.urlApi + 'manager/', {
        login: $login,
        authentication: $password
      })
      .then(function (response) {
        sessionStorage.setItem('manager_token', response.data.manager_token);
        sessionStorage.setItem('login', response.data.login);
        window.location.href =  url; 
      })
      .catch(function (error) {
        validate(error.response.data);
      });
    }, true);
  }

  function validate(error) {

    if ( error.ra || error.authentication || error.login) {

      config.validate('Informe todos os campos.', '[data-js="password"]','error');
    }

    if (error.invalid) {
      config.validate('Login ou senha inválidos.', '[data-js="password"]','error');
    }
  }

})(window.config); 
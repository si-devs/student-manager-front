/*!
 * ManagerStudent
 * Application for the course Sistema para Internet
 * https://gitlab.com/si-devs/student-manager-front
 * @author Jaqueline Paschoal and Robson Formigão
 * @version 1.1.1
 * Copyright 2018. MIT licensed.
 */
(function(){

    'use strict';

    var $buttonLogoutStudent = document.querySelector('[data-js="btn-logout-student"]');
    var $buttonLogoutManager = document.querySelector('[data-js="btn-logout-manager"]');

    if($buttonLogoutStudent) {
      $buttonLogoutStudent.addEventListener('click',function(e) {
        e.preventDefault();
        e.stopPropagation();

        var url = this.href;

        sessionStorage.removeItem('student_token');
        sessionStorage.removeItem('ra');
        sessionStorage.removeItem('id_student');

        window.location.href =  url;
      })
    }

    if($buttonLogoutManager) {
      $buttonLogoutManager.addEventListener('click',function(e) {
        e.preventDefault();
        e.stopPropagation();

        var url = this.href;

        sessionStorage.removeItem('manager_token');
        sessionStorage.removeItem('login');

        window.location.href =  url;
      })
    }

})();
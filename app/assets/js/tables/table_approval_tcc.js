/*!
 * ManagerStudent
 * Application for the course Sistema para Internet
 * https://gitlab.com/si-devs/student-manager-front
 * @author Jaqueline Paschoal and Robson Formigão
 * @version 1.1.1
 * Copyright 2018. MIT licensed.
 */
(function(config){

  'use strict'

  var config = config();  
  var configApi = config.api();

  function getStudent() {
    axios({
      method: 'get',
      url: configApi.urlApi + 'manager/list_students_approval/',
      headers: { 
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      }
    })
    .then(function (response) {
      listStudents(response.data);
      showModal();
      pagination();
      search();
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  function pagination(){
    var $pagination = document.querySelector('.dataTable-pagination');
    $pagination.addEventListener('click',function(){
      setTimeout(showModal,1000);
    })
  }

  function search() {
    var $search = document.querySelector('.dataTable-input');
    $search.addEventListener('keypress', function() {
      setTimeout(showModal, 1000);
    });
  }

  function showModal(){
    var $btnModal = document.querySelectorAll("[data-js='btn-modal']");
    var $modal = document.querySelector("[data-js='modal']");

    Array.prototype.forEach.call($btnModal,function(item){
      item.addEventListener('click', config.showModal);
    });

    $modal.addEventListener('click', config.closeModal);
  }

  function listStudents(students) {
    var options = {
      labels: {
        placeholder: 'Pesquisar...',
        perPage: '{select} alunos por página',
        noRows: 'Nenhum aluno encontrado',
        info: 'Mostrando {start} de {end} de  um total de {rows} alunos'
      },
      data: {
        headings: [
          "RA",
          "NOME",
          "CURSO",
          "INGRESSO",
          "DOCUMENTOS",
          "APROVAR"
        ],
        data: students
      },
      columns: [
        {
          select: 4,
          render: function(data, cell, row) {
              return '<button title="Página pessoal" class="button button_table"><a href="' + data.split(',')[0] + '" target="_blank" ><i class="fal fa-browser"> </i></a></button>' +
                     '<button title="Sistema" class="button button_table"><a href="' + data.split(',')[1] + '" target="_blank" ><i class="fal fa-desktop-alt"> </i></a></button>' +
                     '<button title="TCC" class="button button_table"><a href="' + configApi.urlApi + data.split(',')[2] + '" target="_blank" ><i class="fal fa-book"> </i></a></button>' +
                     '<button title="Folha de aprovação" class="button button_table"><a href="' + configApi.urlApi + data.split(',')[3] + '" target="_blank" ><i class="fal fa-file-alt"> </i></a></button>';
          }
        },
        {
          select: 5,
          render: function(data, cell, row) {
            return '<button title="Aprovar TCC" class="button button_table" data-id="' + data + '" id="btn-modal" data-js="btn-modal"><i id="btn-modal" class="fal fa-file-check"></i></button>'            
          }
        }
      ]
    };
 
    new DataTable('#public_students', options);    
  }

  getStudent();
  
})(window.config)
    
  



/*!
 * ManagerStudent
 * Application for the course Sistema para Internet
 * https://gitlab.com/si-devs/student-manager-front
 * @author Jaqueline Paschoal and Robson Formigão
 * @version 1.1.1
 * Copyright 2018. MIT licensed.
 */
(function (config) {
  
  'use strict';

  var config = config();  
  var configApi = config.api();

  var $buttonSendClass = document.querySelector("[data-js='button-send-class']");
  var formData = new FormData();

  if($buttonSendClass) {
    $buttonSendClass.addEventListener('click', function(e) {
      e.preventDefault();

      var $inputFileClass = document.querySelector("[data-js='input-file-class']").files;

      function sendFile() {
        var formData = new FormData();
        formData.set("file", $inputFileClass[0], $inputFileClass[0].name);

        if (formData.get("file").type === 'text/plain') {
          return config.validate('O arquivo enviado deve ser do tipo CSV.', '[data-js="input-file-class"]','error');
        }

        axios({
          method: 'post',
          url: configApi.urlApi + 'manager/insert_class/',
          headers: { 
            'Content-Type': 'multipart/form-data',
            manager_token: sessionStorage.getItem("manager_token"),
            login: sessionStorage.getItem('login') 
          },
          data: formData, 
          onUploadProgress: function (progressEvent) {
            config.validate('Verificando...','[data-js="input-file-class"]','await');
          }
        })
        .then(function (response) {
          return config.validate('Turma inserida com sucesso!','[data-js="input-file-class"]','success');
        })
        .catch(function (error) {
          validate(error.response.data);
        });
      }

      function validate(error) {  
        
        if(error.fail) {
          return config.validate('Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :(', '[data-js="input-file-class"]','error');
        }

        if(error.invalid) {
          return config.validate('Oops! houve um erro na inserção da turma, tente novamente mais tarde :(', '[data-js="input-file-class"]','error');
        }

        if(error.file[0] === 'The file failed to upload.' || error.file[0] === 'The file field is required.' || error.file[0] === 'The file may not be greater than 1000 kilobytes.') {
          return config.validate('Falha ao enviar o arquivo, verifique seu tamanho. (Tamanho max: 5MB)', '[data-js="input-file-class"]','error');
        }

        if(error.file[0] === 'The file must be a file of type: csv, txt.') {
          return config.validate('O arquivo enviado deve ser do tipo CSV.', '[data-js="input-file-class"]','error');
        }
      }

      function emptyFile() {
        return config.validate('Você precisa selecionar um arquivo.', '[data-js="input-file-class"]','error');
      }

      $inputFileClass.length ? sendFile() : emptyFile();

    });
  }

})(window.config)
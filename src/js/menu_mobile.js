(function (window, document) {

  'use strict';

  var $buttonMobile = document.querySelector('[data-js="button_mobile"]');
  var $headerMobile = document.querySelector('[data-js="header_mobile"]');
  var $iconMobile   =  document.querySelector('[data-js="icon_mobile"]');
 
  function handleMenu() {
    $headerMobile.classList.toggle('show');
    $iconMobile.classList.contains('fa-times') ? $iconMobile.className = 'fal fa-bars' : $iconMobile.className = 'fal fa-times';
  }

  function removeHeaderMenu() {
    var $width = window.innerWidth;
    $width > 768 ? $headerMobile.classList.remove('show') : '';
  }

  $buttonMobile.addEventListener('click',handleMenu,false);
  window.addEventListener('resize', removeHeaderMenu, false);

})(window, document);
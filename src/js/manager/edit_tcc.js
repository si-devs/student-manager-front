(function(config){

  'use strict'

  var config = config();  
  var configApi = config.api();
  var timer = setTimeout(checkButton, 1000); timer;

  function checkButton(){
    var $btnModalTcc = document.querySelectorAll('[data-js="btn-modal-tcc"]');
    $btnModalTcc ? addEvent($btnModalTcc) : timer;
  }
  
  function pagination(){
    var $pagination = document.querySelector('.dataTable-pagination');
    $pagination.addEventListener('click',function(){
      setTimeout(checkButton,1000);
    })
  }

  function search() {
    var $search = document.querySelector('.dataTable-input');
    $search.addEventListener('keypress', function() {
      setTimeout(checkButton, 1000);
    });
  }

  function addEvent(button) {
    clearInterval(timer);
    pagination();
    search();
    Array.prototype.forEach.call(button,function(item){
      item.addEventListener('click', getDataStudent );
    });
  }

  function getDataStudent() {  
    var id_student =  Number(this.getAttribute('data-id'));
    
    axios({
      method: 'get',
      url: configApi.urlApi + 'manager/list_students/' + id_student,
      headers: { 
        'Content-Type': 'application/x-www-form-urlencoded',
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      }
    })
    .then(function (response) {

      var approval = document.querySelector('[data-js="approval"]');
      var send = document.querySelector('[data-js="send"]');

      response.data.approval ? approval.checked = true : approval.checked = false;
      response.data.send ? send.checked = false : send.checked = true;

      document.querySelector('[data-js="id-student"]').value = response.data.id_student;
      document.querySelector('[data-js="name-student"]').value = response.data.name;
      document.querySelector('[data-js="title"]').value = response.data.title ;
      document.querySelector('[data-js="publish"]').value = response.data.publish ;
      document.querySelector('[data-js="tcc-link"]').value = response.data.tcc_link ;
      document.querySelector('[data-js="approval-link"]').value = response.data.approval_link;

      var $buttonUpdateTcc = document.querySelector('[data-js="button-update-tcc"]');
      $buttonUpdateTcc.addEventListener('click', updateTcc, false);

    })
    .catch(function (error) {
      console.log(error.response.data);
    });
  }

  function updateTcc() {
    var id_student = document.querySelector('[data-js="id-student"]').value;
    var name = document.querySelector('[data-js="name-student"]').value;
    var title = document.querySelector('[data-js="title"]').value;
    var publish = document.querySelector('[data-js="publish"]').value;
    var send = document.querySelector('[data-js="send"]').checked? 0 : 1;
    var approval = document.querySelector('[data-js="approval"]').checked ? 1 : 0;
    var tcc_link = document.querySelector('[data-js="tcc-link"]').value;
    var approval_link = document.querySelector('[data-js="approval-link"]').value;
    var fileTcc = document.querySelector('[data-js="file-tcc"]').files;
    var fileApproval = document.querySelector('[data-js="file-approval"]').files;
    var formData = new FormData();

    if(fileTcc.length) {
      formData.append("file", fileTcc[0], fileTcc[0].name);
    }
    if(fileApproval.length) {
      formData.append("fileApproval", fileApproval[0], fileApproval[0].name);
    }

    
    axios({
      method: 'post',
      url: configApi.urlApi + 'manager/update_tcc/' + id_student,
      headers: { 
        'Content-Type': 'multipart/form-data',
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      },
      data: formData,
      params: {
        title: title,
        publish: publish,
        send: send,
        approval: approval,
        tcc_link: tcc_link,
        approval_link: approval_link
      }
    })
    .then(function (response) {
      return config.validate('Dados do TCC alterados com sucesso.','[data-js="file-approval"]','success');
    })
    .catch(function (error) {
      validate(error.response.data);
    });
  }

  function validate(error) { 

    if (error.file || error.fileApproval) {
      return config.validate('O arquivo enviado deve ser do tipo PDF.', '[data-js="file-approval"]','error');
    }

    if (error.publish) {
      return config.validate('Informe uma data valida (2018 / 18) ou 0 para não informar', '[data-js="file-approval"]','error');
    }

    if (error.send || error.approval) {
      return config.validate('Os campos "Autorizado para envio" e "Aprovado" precisam ser enviados.', '[data-js="file-approval"]','error');
    }
        
    if(error.fail) {
      return config.validate('Erro ao atualizar TCC.', '[data-js="file-approval"]','error');
    }
  }

})(window.config)
(function(config){

  'use strict'

  var config = config();  
  var configApi = config.api();
  var timer = setTimeout(checkButton, 1000); timer;

  function checkButton(){
    var $btnModal = document.querySelectorAll('[data-js="btn-modal"]');
    $btnModal ? addEvent($btnModal) : timer;
  }

  function pagination(){
    var $pagination = document.querySelector('.dataTable-pagination');
    $pagination.addEventListener('click',function(){
      setTimeout(checkButton,1000);
    })
  }

  function search() {
    var $search = document.querySelector('.dataTable-input');
    $search.addEventListener('keypress', function() {
      setTimeout(checkButton, 1000);
    });
  }  

  function addEvent(button) {
    clearInterval(timer);
    pagination();
    search();
    Array.prototype.forEach.call(button,function(item){
      item.addEventListener('click', getDataStudent );
    });
  }

  function approvalTcc() {
    var id_student = document.querySelector('[data-js="id"]').value;
    var $inputFileApproval = document.querySelector("[data-js='input-file-approval']").files;

    $inputFileApproval.length ? sendFile() : emptyFile();

    function sendFile() {
      var formData = new FormData();
      formData.set("file", $inputFileApproval[0], $inputFileApproval[0].name);
      axios({
        method: 'post',
        url: configApi.urlApi + 'manager/approval_tcc/' + id_student,
        headers: { 
          'Content-Type': 'multipart/form-data',
          manager_token: sessionStorage.getItem("manager_token"),
          login: sessionStorage.getItem('login') 
        },
        data: formData, 
        onUploadProgress: function (progressEvent) {
          config.validate('Verificando...','[data-js="input-file-approval"]','await');
        }
      })
      .then(function (response) {
        return config.validate('TCC aprovado com sucesso.','[data-js="input-file-approval"]','success');
      })
      .catch(function (error) {
        validate(error.response.data);
      });
    }
  }

  function getDataStudent() {       
    var id_student =  Number(this.getAttribute('data-id'));
    axios({
      method: 'get',
      url: configApi.urlApi + '/manager/list_students_approval/' + id_student,
      headers: { 
        'Content-Type': 'application/x-www-form-urlencoded',
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      }
    })
    .then(function (response) {
      var $linkTcc = document.querySelector('[data-js="link_tcc"]');
      var $btnApproval = document.querySelector('[data-js="button-approval"]');    
      
      document.querySelector('[data-js="id"]').value = response.data.id_student;
      document.querySelector('[data-js="ra"]').value = response.data.ra ;
      document.querySelector('[data-js="name"]').value = response.data.name;
      
      $linkTcc.setAttribute('href', configApi.urlApi + response.data.tcc_link);
      $btnApproval.addEventListener('click', approvalTcc, false);
    })
    .catch(function (error) {
      console.log(error.response.data);
    });
  }

  function validate(error) {  
        
    if(error.fail) {
      return config.validate('Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :(', '[data-js="input-file-approval"]','error');
    }

    if(error.invalid) {
      return config.validate('Não foi possivel realizar a aprovação do TCC.', '[data-js="input-file-approval"]','error');
    }

    if(error.file[0] === 'The file failed to upload.' || error.file[0] === 'The file field is required.' || error.file[0] === 'The file may not be greater than 1000 kilobytes.') {
      return config.validate('Falha ao enviar o arquivo, verifique seu tamanho. (Tamanho max: 5MB)', '[data-js="input-file-approval"]','error');
    }

    if(error.file[0] === 'The file must be a file of type: pdf.') {
      return config.validate('O arquivo enviado deve ser do tipo PDF.', '[data-js="input-file-approval"]','error');
    }
  }

  function emptyFile() {
    return config.validate('Você precisa selecionar um arquivo.', '[data-js="input-file-approval"]','error');
  }

})(window.config)
(function(config){

  'use strict'

  var config = config();  
  var configApi = config.api();
  var timer = setTimeout(checkButton, 1000); timer;

  function checkButton(){
    var $btnModal = document.querySelectorAll('[data-js="btn-modal"]');
    $btnModal ? addEvent($btnModal) : timer;
  }

  function pagination(){
    var $pagination = document.querySelector('.dataTable-pagination');
    $pagination.addEventListener('click',function(){
      setTimeout(checkButton,1000);
    })
  }

  function search() {
    var $search = document.querySelector('.dataTable-input');
    $search.addEventListener('keypress', function() {
      setTimeout(checkButton, 1000);
    });
  }

  function addEvent(button) {
    clearInterval(timer);
    pagination();
    search();
    Array.prototype.forEach.call(button,function(item){
      item.addEventListener('click', getDataStudent );
    });
  }

  function getDataStudent() {  
    var id_student =  Number(this.getAttribute('data-id'));
    
    axios({
      method: 'get',
      url: configApi.urlApi + 'manager/list_students/' + id_student,
      headers: { 
        'Content-Type': 'application/x-www-form-urlencoded',
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      }
    })
    .then(function (response) {

      var $buttonUpdateStudent = document.querySelector('[data-js="button-update-student"]');

        document.querySelector('[data-js="id"]').value = response.data.id_student;
        document.querySelector('[data-js="ra"]').value = response.data.ra ;
        document.querySelector('[data-js="name"]').value = response.data.name;
        document.querySelector('[data-js="auth"]').value = response.data.authentication;
        document.querySelector('[data-js="page-link"]').value = response.data.page_link;
        document.querySelector('[data-js="system-link"]').value = response.data.system_link;

        $buttonUpdateStudent.addEventListener('click', updateStudent, false);

    })
    .catch(function (error) {
      console.log(error.response.data);
    });
  }

  function updateStudent() {
    var id_student = document.querySelector('[data-js="id"]').value;
    var ra = document.querySelector('[data-js="ra"]').value;
    var name = document.querySelector('[data-js="name"]').value;
    var authentication = document.querySelector('[data-js="auth"]').value;
    var page_link = document.querySelector('[data-js="page-link"]').value;
    var system_link = document.querySelector('[data-js="system-link"]').value;

    axios({
      method: 'put',
      url: configApi.urlApi + 'manager/list_students/' + id_student,
      headers: { 
        'Content-Type': 'multipart/form-data',
        manager_token: sessionStorage.getItem("manager_token"),
        login: sessionStorage.getItem('login') 
      },
      params: {
        ra: ra,
        name: name,
        authentication: authentication,
        page_link: page_link,
        system_link: system_link
      }
    })
    .then(function (response) {
      return config.validate('Dados do aluno alterados com sucesso.','[data-js="system-link"]','success');
    })
    .catch(function (error) {
      validate(error.response.data);
    });
  }

  function validate(error) { 

    if (error.ra || error.name || error.authentication || error.page_link || error.system_link) {
      return config.validate('Informe todos os campos.', '[data-js="system-link"]','error');
    }
        
    if(error.fail) {
      return config.validate('Erro ao atualizar estudante.', '[data-js="system-link"]','error');
    }
  }

})(window.config)
(function(config){

  'use strict'

  var config = config();  
  var timer = setTimeout(checkPagination, 1000); timer;

  function checkPagination(){
    var $pagination = document.querySelector('.dataTable-pagination');
    $pagination ? addEvent($pagination) : timer;
  }

  function addEvent(pagination) {
    clearInterval(timer);

    pagination.addEventListener('click', function(){
      setTimeout(function(){
        var $btnModal = document.querySelectorAll("[data-js='btn-modal']");
        var $btnModalTcc = document.querySelectorAll("[data-js='btn-modal-tcc']");
        var $modal = document.querySelector("[data-js='modal']");
        var $modalTcc = document.querySelector("[data-js='modal-tcc']");
    
        Array.prototype.forEach.call($btnModal,function(item){
          console.log(item);
          item.addEventListener('click', config.showModal);
        });
    
        Array.prototype.forEach.call($btnModalTcc,function(item){
          item.addEventListener('click', config.showModal);
        });
    
        $modal.addEventListener('click', config.closeModal);
        $modalTcc.addEventListener('click', config.closeModal);
      }, 1000);
      
    })
  }
})(window.config)
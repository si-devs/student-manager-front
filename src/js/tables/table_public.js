(function(config){

  'use strict'

  var config = config();  
  var configApi = config.api();

  function listStudents() {
    var options = {
      labels: {
        placeholder: 'Pesquisar...',
        perPage: '{select} Alunos por página',
        noRows: 'Nenhum aluno encontrado',
        info: 'Mostrando {start} de {end} de  um total de {rows} alunos'
      },
      ajax: configApi.urlApi + 'home',
      columns: [
        {
          select: 4,
          render: function(data, cell, row) {
              return '<button title="Página pessoal" class="button button_table"><a href="' + data.split(',')[0] + '" target="_blank" ><i class="fal fa-browser"> </i></a></button>' + 
                     '<button title="Sistema" class="button button_table"><a href="' + data.split(',')[1] + '" target="_blank" ><i class="fal fa-desktop-alt"> </i></a></button>' + 
                     '<button title="TCC" class="button button_table"><a href="' + configApi.urlApi + data.split(',')[2] + '" target="_blank" ><i class="fal fa-book"> </i></a></button>' +
                     '<button title="Folha de aprovação" class="button button_table"><a href="' + configApi.urlApi + data.split(',')[3] + '" target="_blank" ><i class="fal fa-file-alt"> </i></a></button>';
          }
        }
      ]
    };
 
    new DataTable('#public_students', options);  
  }

  listStudents();

 
  
})(window.config)
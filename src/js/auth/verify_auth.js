(function (config) {

  'use strict';

  var config = config();  
  var configFront = config.front();
  var configApi = config.api();
  var configStudents = config.studentsUrl();
  var configManager= config.managerUrl();

  var $studentLink = document.querySelectorAll('[data-js="student"]');
  var $managerLink = document.querySelectorAll('[data-js="manager"]');
  var urlBase = window.location.href;
  
  function verifyAuthenticationManager( url ) {
    var config = {
      headers: {
        'manager_token': sessionStorage.getItem("manager_token"),
        'login': sessionStorage.getItem('login')
      }
    };

    axios.get(configApi.urlApi +  'manager/auth', config)
    .then(function (response) {
      url ? window.location.href = url : '';
      
      var $load = document.querySelector('[data-js="load"]');
      $load ? removeLoad($load) : '';

    })
    .catch(function (error) {
      if ( window.location.href !== configFront.urlFront + 'pages/auth_manager.html' ) {
        window.location.href = configFront.urlFront + 'pages/auth_manager.html';
      }
    });
  }

  function verifyAuthenticationStudent( url ) {
    var config = {
      headers: {
        'student_token': sessionStorage.getItem("student_token"),
        'ra': sessionStorage.getItem('ra')
      }
    };

    axios.get(configApi.urlApi +  'student/auth', config)
    .then(function (response) {
      url ? window.location.href = url : '';
      
      var $load = document.querySelector('[data-js="load"]');
      $load ? removeLoad($load) : '';
    })
    .catch(function (error) {
      if ( window.location.href !== configFront.urlFront + 'pages/auth_student.html' ) {
        window.location.href = configFront.urlFront + 'pages/auth_student.html';
      }
    });
  }

  function removeLoad (load) {
    load.remove();
  }

  if($managerLink) {
    Array.prototype.map.call($managerLink, function(itemMenu) {
      itemMenu.addEventListener('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        verifyAuthenticationManager( e.target.href );
      })
    })
  }

  if ($studentLink) {
    Array.prototype.map.call($studentLink, function(itemMenu) {
      itemMenu.addEventListener('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        verifyAuthenticationStudent( e.target.href );
      });
    })
  }

  if(urlBase === configFront.urlFront + 'pages/auth_student.html' && sessionStorage.getItem("student_token") !== null && sessionStorage.getItem("ra") !== null)  {
    var url = configFront.urlFront + 'pages/options_student.html'
    verifyAuthenticationStudent(url);
  }

  if(urlBase === configFront.urlFront + 'pages/auth_manager.html' && sessionStorage.getItem("manager_token") !== null && sessionStorage.getItem("login") !== null)  {
    var url = configFront.urlFront + 'pages/options_manager.html'
    verifyAuthenticationManager(url);
  }

  if(urlBase === configFront.urlFront + 'pages/auth_student.html' && sessionStorage.getItem("student_token") === null && sessionStorage.getItem("ra") === null)  {
    return;
  }

  if(urlBase === configFront.urlFront + 'pages/auth_manager.html' && sessionStorage.getItem("manager_token") === null && sessionStorage.getItem("login") === null)  {
    return;
  }

  if ( urlBase !== configFront.urlFront + '') {
    if(configStudents.indexOf(urlBase) != -1)
    {  
      verifyAuthenticationStudent();
    }

    if(configManager.indexOf(urlBase) != -1)
    {  
      verifyAuthenticationManager();
    }

  }

})(window.config);


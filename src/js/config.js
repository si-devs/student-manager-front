(function(win, doc) {

  'use strict';

  function config() {
    return {

      api: function api() {
        return { urlApi: 'http://localhost:8000/' }
      },

      front: function front() {
        return { urlFront: 'http://localhost:3000/' }
      },

      studentsUrl: function studentsUrl() {
        var url = this.front();
        return [
          url.urlFront + 'pages/options_student.html',
          url.urlFront + 'pages/send_tcc.html',
          url.urlFront + 'pages/student_account.html',
        ];
      },

      managerUrl: function managerUrl() {
        var url = this.front();
        return [
          url.urlFront + 'pages/options_manager.html',
          url.urlFront + 'pages/add_class.html',
          url.urlFront + 'pages/approval_tcc.html',
          url.urlFront + 'pages/list_students.html',
        ];
      },

      validate: function validate(msg,element,type) {
        this.clear();
        
        var $element = document.querySelector(element);
        var $createElement = document.createElement('p');
        var $message = document.createTextNode(msg);
    
        $createElement.className = type;
        $createElement.appendChild($message);
    
        $element.insertAdjacentElement('afterend', $createElement);
      },

      clear: function clear() {
        var $error = document.querySelector('.error');
        var $success = document.querySelector('.success');
        var $await = document.querySelector('.await');

        $error ? $error.remove() : '';
        $success ? $success.remove() : '';
        $await ? $await.remove() : '';
      }, 
      showModal: function showModal(e) {
        var $modal = document.querySelector('[data-js="modal"]');
        var $modalTcc = document.querySelector('[data-js="modal-tcc"]');

        config().clear();

        if(e.target.id === 'btn-modal') {
          $modal.classList.add('show');
        }

        if(e.target.id === 'btn-modal-tcc') {
          $modalTcc.classList.add('show');
        }
      },

      closeModal: function closeModal(e) {
        if(e.target.id === 'modal' || e.target.id==='close') {
          this.classList.remove('show');
        }

        if(e.target.id === 'modal-tcc' || e.target.id==='close') {
          this.classList.remove('show');
        }
      }
    }
  }

  win.config = config;

})(window, document);
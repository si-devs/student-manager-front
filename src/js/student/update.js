(function(config) {

  'use strict';
  var config = config();  
  var configApi = config.api();
  
  var $buttonUpdateStudent = document.querySelector('[data-js="btn-update-student"]'); 

  if($buttonUpdateStudent) {
    $buttonUpdateStudent.addEventListener('click', function(e) {
      e.preventDefault();

      var $inputOldAuthentication = document.querySelector('[data-js="input-old-authentication"]').value; 
      var $inputNewAuthentication = document.querySelector('[data-js="input-new-authentication"]').value; 
      var $inputConfirmAuthentication = document.querySelector('[data-js="input-confirm-authentication"]').value;
      var id_student = sessionStorage.getItem('id_student');

      axios({
        method: 'put',
        url: configApi.urlApi + 'student/profile/' + id_student,
        headers: { 
          'Content-Type': 'application/x-www-form-urlencoded',
          student_token: sessionStorage.getItem("student_token"),
          ra: sessionStorage.getItem('ra') 
        },
        params: {
          authentication: $inputNewAuthentication,         
          authentication_confirmation: $inputConfirmAuthentication,           
          old_authentication: $inputOldAuthentication  
        }
      })
      .then(function (response) {
        config.validate('Senha alterada com sucesso!','[data-js="input-confirm-authentication"]', 'success');
      })
      .catch(function (error) {
        validate(error.response.data);
      });
      
    }, true);
  }

  function validate(error) {

    if ( error.invalid ) {

      return config.validate('Senha atual não confere :(', '[data-js="input-confirm-authentication"]','error');
    }

    if( error.authentication[0]  === "The authentication confirmation does not match." ) {
      return config.validate('Confirmação de nova senha incorreta.', '[data-js="input-confirm-authentication"]','error');
    }
    
    if ( error.old_authentication || error.authentication ) {
      return config.validate('Informe todos os campos.', '[data-js="input-confirm-authentication"]','error');
    }

  }

})(window.config)